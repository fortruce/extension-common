// Get all html elements
var _loginButton = document.getElementById('login_button');
var _logoutButton = document.getElementById('logout_button');
var _registerButton = document.getElementById('register_button');

// Setup listeners
_loginButton.addEventListener('click', function(event){ OnLogin_Click(); });
_logoutButton.addEventListener('click', function(event){ OnLogout_Click(event); });
_registerButton.addEventListener('click', function(event){ OnClick_Register(event); });

document.addEventListener('DOMContentLoaded', async function(event) {
	var result = await browser.storage.sync.get(['username', 'password']);
	if("username" in result && "password" in result) {
		DisplayUserPage(result.username);
	} else {
		DisplayLoginPage();
	}
});

if(typeof browser === "undefined") {
	var browser = chrome;
}

function OnLogin_Click(){
	var _username = document.getElementById('login').value;
	var _password = document.getElementById('password').value;

	SetLoadingIndicator(true);

	Login(_username, _password);
}

function Login(username, password) {
	FioCheckCredentials(username, password, (success) =>{
		if(success){
			SaveCredentials(username, password);
			DisplayUserPage(username);
		}else{
			alert("Login was not successful");
			browser.storage.sync.remove(['username', 'password']);
		}
		SetLoadingIndicator(false);
	});
}

function OnLogout_Click(event){
	if(event.type == "click"){
		browser.storage.sync.remove([
			'username',
			'password'],
			function(result) {});

		var _password = document.getElementById('password').value;
		_password.value = "";
	
		DisplayLoginPage();
	}
}

async function OnClick_Register(event)
{
	console.log("Registering");
	browser.storage.local.get(
		[
			'reg_username',
			'reg_guid'
		],
		function(result) {
			if(result.reg_username != null && result.reg_guid != null)
			{
				OpenRegistrationPage(result.reg_username, result.reg_guid);
			}
		}
	);
}

async function UpdateRegistrationButtonMessage()
{
	return browser.storage.local.get(
		[
			'reg_username',
			'reg_guid'
		],
		function(result) {
			if(result.reg_username != null && result.reg_guid != null)
			{
				DisplayRegistrationMessage("You should register for FIO.", true);
				return true;
			}
			else if (result.reg_username != null)
			{
				DisplayRegistrationMessage("You appear to already be registered.", false);
			}
			else
			{
				DisplayRegistrationMessage("Unable to locate APEX data.  Are you logged in to APEX?", false);
			}
			return false;
	   }
	);
}

// run on load
UpdateRegistrationButtonMessage();

function OpenRegistrationPage(reg_username, reg_guid)
{
	console.log("Need to open new tab");
	let reg_url = "https://fio.fnar.net/register" + 
		"?UserName=" + encodeURIComponent(reg_username) + 
		"&RegistrationGuid=" + encodeURIComponent(reg_guid);
	browser.tabs.create({ url: reg_url, active:false }, function(tab)
	{
		browser.tabs.update(tab.id, {active:true});
	});
}

function DisplayRegistrationMessage(message, show_button)
{
	document.getElementById('register_message').textContent = message;
	if(show_button) {
		_registerButton.style.visibility = "visible";
	}
	else {
		_registerButton.style.visibility = "hidden";
	}
}

function SetLoadingIndicator(isLoading)
{
	var _loadingIndicator = document.getElementById('loadingindicator');
	
	if(isLoading){
		_loginButton.disabled = true;
		_loadingIndicator.style.visibility = "visible";
	}else{
		_loginButton.disabled = false;
		_loadingIndicator.style.visibility = "hidden";
	}
}


// Display the login page (only if user authentication fails)
async function DisplayLoginPage()
{
	document.getElementById('LoginPage').classList.remove('hidden');
	document.getElementById('UserPage').classList.add('hidden');
	await browser.runtime.sendMessage({message: "logout"});
}

// Displays the logged in user data
async function DisplayUserPage(username)
{
	document.getElementById('LoginPage').classList.add('hidden');
	document.getElementById('UserPage').classList.remove('hidden');
	
	document.getElementById('GreetingsText').innerText = "Welcome " + username;
	document.getElementById('InfoText').innerText = "You are already logged in. Everything should work as expected";
	await browser.runtime.sendMessage({message: "login"});
}

var currentDatabaseVersion = 2;

/*
	Handles credentials loading from and to storage
*/
function LoadCredentials(callback){
	browser.storage.sync.get(['databaseVersion'], function(result) {
		if(result.databaseVersion != null && result.databaseVersion == currentDatabaseVersion) {
			browser.storage.sync.get([
				'username',
				'password'],
				function(result) {
					if(result.username != null && result.password != null){
						_username = result.username;
						_password = result.password;
						callback(true);
					}
					else{
						callback(false);
					}
			   });
		}
		else {
			browser.storage.sync.clear(function(result){});
			callback(false);
		}
	});
}

function SaveCredentials(username, password){
	browser.storage.sync.set({
		"databaseVersion": currentDatabaseVersion, 
		"username": username, 
		"password" : password
	}, function(){});
}
/*
	Function will check the credentials with the API. The callback will get called with the return success true/false
*/
async function FioCheckCredentials(fnar_username, fnar_password, callback)
{
    var data = 
	{
        "UserName": fnar_username,
        "Password": fnar_password
    };

	var fnar_url = "https://rest.fnar.net";
    var url = fnar_url + "/auth/login";

	var response = fetch(url, {
        method: "POST",
        mode: "cors",
        credentials: 'omit',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then((response) => {
        // returned.  Since 'ok' contains success/fail (200 or 401, basically), use it.
		callback(response.ok);
    })
    .catch((error) => {
        console.log(error);
    });
}

