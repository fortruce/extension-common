var fnar_url = "https://rest.fnar.net";

//
// functions to get logged in & send data.
//

if(typeof browser === "undefined") {
    var browser = chrome;
}

var eventQueue = [];
var processingqueue = false;

// TODO: LocalStorage - Needs moving.
var fnar_auth_token = "";
var fnar_known_auth_failed = false;
var fnar_is_admin = false;

// Send a message object to the content script.
async function sendContentMessage(msgObject)
{
	await browser.tabs.query({currentWindow: true, active: true},
		async function(tabs) {
			if(tabs.length > 0)
				await browser.tabs.sendMessage(tabs[0].id, msgObject);
		}
	);
}

async function fnar_login(turl, tdata)
{
	if(typeof browser === "undefined") {
		var browser = chrome;
	}
	
    if (fnar_known_auth_failed)
    {
        return;
    }

    var logindata = await browser.storage.sync.get({
        "username": "",
        "password": ""
    });
    var data = {
		'UserName': logindata.username,
		'Password': logindata.password
	};

    if(typeof data === "undefined" || typeof data.UserName === "undefined" || data.UserName === "")
    {
        console.log("Invalid username");
        return false;
    }

    var url = fnar_url + "/auth/login";

    var response = await fetch(url, {
        method: "POST",
        mode: "cors",
        credentials: 'omit',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(async (response) => {
		if(response.ok)
		{
			console.debug("Logged in, sending data;");
			return response.json();
		}
		if(response.status == 401)
		{
			await sendContentMessage({
				message: "logon_failure", 
				details: "Problem with logon credentials.  Please verify that they are correct."
			});
			throw 401;
		}
		throw ("Failed to login, cannot send data.  Please retry later.");
    })
	.then(async (jsonresult) => {
		fnar_auth_token = jsonresult.AuthToken;
		fnar_is_admin = jsonresult.IsAdministrator;
		return jsonresult;
	})
    .catch(async (error) => {
        //console.log(error);
		// purge the queue, as we've failed.
		eventQueue.splice(0, eventQueue.length);
		if(error !== 401)
		{
			await sendContentMessage({
				message: "logon_failure",
				details: "Logon failure: " + error,
			});
		}
		return undefined;
    });
	return response;
}

async function fnar_renew_or_reauth_then(url, data)
{
	var resp = await fnar_login();
	if (resp !== undefined)
	{
		await fnar_do_send_xhttp_request(url, data);
	}
}

async function send_fnar_xhttp_request(url, data)
{
	//console.debug("fnar_xhttp_request: " + url);
	if (typeof fnar_auth_token === "undefined" || fnar_auth_token === "")
	{
		var resp = await fnar_login();
		if (resp !== undefined)
		{
			await fnar_do_send_xhttp_request(url, data);
		}
	}
	else
	{
		await fnar_do_send_xhttp_request(url, data);
	}

}

async function fnar_do_send_xhttp_request(url, data)
{
	if (typeof fnar_auth_token === "undefined" || fnar_auth_token === "")
	{
		console.warn("No auth token found.");
		return;
	}
	
    await fetch(url, {
        method: "POST",
        mode: "cors",
        credentials: 'omit',
        headers: {
			'Authorization': fnar_auth_token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(async (response) => {
		if(response.status === 401)
		{
			// Theoretically, this can't be recursive, since we'll fail
			// the renewal and not return here.
			console.warn("Renewing / Reauthenticating");
			await fnar_renew_or_reauth_then(url, data);
		}
		else if (response.ok == true)
		{
			// console.log("Responded ok");
		}
		else
		{
			console.log("Failure for some reason... " + response.status);
		}
    })
    .catch((error) => {
        console.log(error);
    });
}

var storage_location = browser.storage.local;
if(browser.storage.session && typeof(browser.storage.session) !== "undefined")
{
	storage_location = browser.storage.session;
}
async function ResetValidChannelIds()
{
	// just want to empty it out.
	await SaveValidChannelIds([]);
}

async function GetValidChannelIds()
{
	//console.log("GetValidChannelIds()");
	var result = await storage_location.get("validChannelIds");
	//console.log(result);
	return result;
}
function SaveValidChannelIds(validChannelIds)
{
	console.debug("SaveValidChannelIds: " + validChannelIds);
	if(validChannelIds === null || validChannelIds === undefined)
	{
		console.warn("Not bothering to save invalid channel IDs");
	}
	else
	{
		var dataSet = {"validChannelIds": validChannelIds};
		storage_location.set(dataSet, function() {});
	}
}

async function ProcessEvent(eventdata, event_list, full_event)
{
	if (typeof eventdata === undefined || eventdata === null || typeof (eventdata.messageType) === "undefined")
	{
		console.warn("Tried to process a bad eventdata:");
		console.warn(eventdata);
		return;
	}
	if (eventdata.messageType in event_list)
	{
		console.debug("Event to process: " + eventdata.messageType);
		if(typeof full_event === "undefined")
		{
			full_event = eventdata;
		}
		var match_event = event_list[eventdata.messageType];
		if(typeof match_event === "undefined") {
			console.error("messagetype should be in list, but we still failed?");
		}

		if(typeof match_event !== "undefined" && match_event.req_admin == true && fnar_is_admin == false)
		{
			console.debug("Not sending admin data; not an admin");
			return;
		}

		if(match_event.action == "subprocess_payload")
		{
			await ProcessEvent(eventdata.payload.message, match_event.payload_events, full_event);
		}
		else if(match_event.action == "send_payload") 
		{
			await send_fnar_xhttp_request(fnar_url + match_event.suburl, full_event);
		}
		else if(match_event.action == "process_by_path")
		{
			await ProcessEventByPath(eventdata, match_event.paths, full_event);
		}
		else if(match_event.action == "process_chat_special")
		{
			console.debug("Event action: process_chat_special (evaluate channel record)");
			if (eventdata.payload)
			{
				var eventDataPayload = eventdata.payload;
				console.debug("Evaluating channel " + eventDataPayload.channelId + " / " + eventDataPayload.displayName);
				if (match_event.group_regex_match && match_event.group_regex_match.length > 0)
				{
					var regex = new RegExp("(" + match_event.group_regex_match.join("|") + ")", "i");
					if ((eventDataPayload.type === "PUBLIC") || (eventDataPayload.type === "GROUP" && eventDataPayload.displayName && eventDataPayload.displayName.match(regex)))
					{
						await GetValidChannelIds()
						.then(
							async result => 
							{
								var validChannelIds = result.validChannelIds;
								console.debug("Finding/saving channel...");
								if (typeof validChannelIds == "undefined")
								{
									validChannelIds = [];
								}
								if (validChannelIds.indexOf(eventDataPayload.channelId) === -1)
								{
									console.debug("Added channel " + eventDataPayload.channelId + " / " + eventDataPayload.displayName);
									validChannelIds.push(eventDataPayload.channelId);
									await send_fnar_xhttp_request(fnar_url + match_event.suburl, full_event);
									await SaveValidChannelIds(validChannelIds);
								}
							});
					}
				}
			}
		}
		else if(match_event.action == "send_chat_if")
		{
			await GetValidChannelIds()
			.then(async result => 
				{
					if (typeof result.validChannelIds !== "undefined" && result.validChannelIds.indexOf(eventdata.payload.channelId) >= 0)
					{
						// just send the eventdata value, as this function operates for
						// both received messages as well as sending your own message
						// which have two different message structures (but identical 
						// leaf-level payloads)
						console.debug("Sending message to channel: " + eventdata.payload.channelId);
						await send_fnar_xhttp_request(fnar_url + match_event.suburl, full_event);
					}
					else
					{
						console.debug("Ignoring message to channel: " + eventdata.payload.channelId);
					}
				});
		}
		else
		{
			//console.debug("Event found, but no match action: " + eventdata.messageType);
		}
	}
	else
	{
		//console.debug("Event not found: " + eventdata.messageType);
	}

}

async function ProcessEventByPath(eventdata, path_list, full_event)
{
	if (typeof eventdata.payload === "undefined" || typeof eventdata.payload.path === "undefined")
	{
		console.error("DATA_DATA missing a payload/path");
		return;
	}

	for(var idx in path_list)
	{
		var mpath = path_list[idx];
		var epath = eventdata.payload.path;
		if (epath.length != mpath.count)
		{
			continue;
		}

		var found_match = true;
		for (var pos in mpath.matches)
		{
			var val = mpath.matches[pos];
			if(epath[pos] !== val)
			{
				found_match = false;
			}
		}
		if(found_match)
		{
			if(mpath.req_admin == true && fnar_is_admin == false)
			{
				console.debug("Not sending admin data; not an admin");
				return;
			}
			if(mpath.action === "send_payload") 
			{
				await send_fnar_xhttp_request(fnar_url + mpath.suburl, full_event);
			}
			return;
		}
	}
}

function ProcessMessage(event)
{
	var outmsg = '';
	// Do stuff with event.data (received data).
	var re_event = /^[0-9:\s]*(?<event>\[\s*"event".*\])[\s0-9:\.]*/m;
    //console.log(re_event);
    //console.log(event.data);
	var result = event.data.match(re_event);
    //console.log(result);
	if (result && result.groups && result.groups.event)
	{
		var eventdata = JSON.parse(result.groups.event)[1];
		//console.log("Event found");
		//console.log(eventdata);

		QueueEvent(eventdata, transmitted_events);
    }
}

async function QueueEvent(eventdata)
{
	//console.debug("Queue event; eventQueue.size " + eventQueue.length);
	//console.debug("Queue event; processing? " + processingqueue);

	eventQueue.push(eventdata);
	if (processingqueue === false)
	{
		processingqueue = true;
		//console.debug("Queue event processing; queue size? " + eventQueue.length);

		currentEvent = eventQueue.shift();
		while(currentEvent !== undefined)
		{
			await ProcessEvent(currentEvent, transmitted_events);
			//console.log("Queue event processing check; queue size? " + eventQueue.length);
			currentEvent = eventQueue.shift();
		}

		processingqueue = false;
	}
}
